﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public int peopleWaiting;

    public static int points;

    public Text pointsText;

    void Start()
    {
        
    }

    void Update()
    {
        if(peopleWaiting >= 10)
        {
            SceneManager.LoadScene("LoseMenu");
        }
    }

    public void GetPoints()
    {
        points += 50;
    }

    public void SetPointsText()
    {
        pointsText.text = "Puntos "  + points.ToString();
    }
}
