﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ascensor : MonoBehaviour
{
    public int currentPeopleOn = 0;
    public int maxPeople = 6;
    public bool canEnter;
    public bool isGrounded;

    public float speed;
    public Transform floor1;
    public Transform floor2;
    public Transform floor3;
    public Transform floor4;
    public Transform floor5;
    public Transform floor6;
    public Transform floor7;
    public Transform floor8;
    public Transform floor9;
    public Transform floor10;

    public int target;

    public int currentFloor;

    void Start()
    {
        canEnter = true;
        isGrounded = true;
    }

    void Update()
    {
        if(currentPeopleOn >= maxPeople)
        {
            Debug.Log("lleno");
            canEnter = false;
        }
        else
        {
            canEnter = true;
        }

        if (target == 1)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor1.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor1.position) <= 0)
            {
                isGrounded = true;
                currentFloor = 1;
            }

        }
        if (target == 2)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor2.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor2.position) <= 0)
            {
                isGrounded = false;
                currentFloor = 2;
            }
        }
        if (target == 3)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor3.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor3.position) <= 0)
            {
                isGrounded = false;
                currentFloor = 3;
            }
        }
        if (target == 4)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor4.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor4.position) <= 0)
            {
                isGrounded = false;
                currentFloor = 4;
            }
        }
        if (target == 5)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor5.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor5.position) <= 0)
            {
                isGrounded = false;
                currentFloor = 5;
            }
        }
        if (target == 6)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor6.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor6.position) <= 0)
            {
                isGrounded = false;
                currentFloor = 6;
            }
        }
        if (target == 7)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor7.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor7.position) <= 0)
            {
                isGrounded = false;
                currentFloor = 7;
            }
        }
        if (target == 8)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor8.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor8.position) <= 0)
            {
                isGrounded = false;
                currentFloor = 8;
            }
        }
        if (target == 9)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor9.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor9.position) <= 0)
            {
                isGrounded = false;
                currentFloor = 9;
            }
        }
        if (target == 10)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor10.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor10.position) <= 0)
            {
                isGrounded = false;
                currentFloor = 10;
            }
        }
    }

    public void MoveToFloor1()
    {
        target = 1;
    }
    public void MoveToFloor2()
    {
        target = 2;
    }
    public void MoveToFloor3()
    {
        target = 3;
    }
    public void MoveToFloor4()
    {
        target = 4;
    }
    public void MoveToFloor5()
    {
        target = 5;
    }
    public void MoveToFloor6()
    {
        target = 6;
    }
    public void MoveToFloor7()
    {
        target = 7;
    }
    public void MoveToFloor8()
    {
        target = 8;
    }
    public void MoveToFloor9()
    {
        target = 9;
    }
    public void MoveToFloor10()
    {
        target = 10;
    }
}
