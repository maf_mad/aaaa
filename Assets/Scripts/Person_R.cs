﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Person_R : MonoBehaviour
{
    public Vector2 firstPoint;
    public Transform ascensorPosition;
    public float speed = 2f;
    bool reachedFirstPoint = false;
   
    Ascensor ascensorComp;
    Manager managerComp;

    public int targetFloor;
    public bool isWaiting;
    public bool isOnElevator = false;

    public Text playerText;

    void Start()
    {
        ascensorPosition = GameObject.Find("Ascensor2").GetComponent<Transform>();
        ascensorComp = GameObject.Find("Ascensor2").GetComponent<Ascensor>();
        managerComp = GameObject.Find("GameManager").GetComponent<Manager>();
        isWaiting = false;

        targetFloor = Random.Range(2, 11);
        SetPlayerText();
    }

    void Update()
    {
        if(reachedFirstPoint == false)
        {
            MoveToFirstPoint();
        }
        if (ascensorComp.canEnter == true && ascensorComp.isGrounded == true && isWaiting == true)
        {
            managerComp.peopleWaiting--;
            Entrar();
            Debug.Log("CurrentPeopleWaiting: " + managerComp.peopleWaiting);
        }
        if(isOnElevator == true)
        {
            gameObject.transform.SetParent(ascensorPosition);
            if(targetFloor == ascensorComp.currentFloor)
            {
                ascensorComp.currentPeopleOn--;
                managerComp.GetPoints();
                managerComp.SetPointsText();
                Destroy(gameObject);
            }
        }
    }

    void MoveToFirstPoint()
    {
        transform.position = Vector2.MoveTowards(transform.position, firstPoint, speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, firstPoint) <= 0)
        {
            reachedFirstPoint = true;
            Debug.Log("Llegó");
            if(ascensorComp.canEnter == true && ascensorComp.isGrounded == true)
            {
                Entrar();
            }
            else
            {
                Esperar();
                managerComp.peopleWaiting++;
                Debug.Log("CurrentPeopleWaiting: " + managerComp.peopleWaiting);
            }
        }
    }
    void Entrar()
    {
        transform.position = ascensorPosition.transform.position;
        ascensorComp.currentPeopleOn++;
        isWaiting = false;
        isOnElevator = true;
    }
    void Esperar()
    {
        isWaiting = true;
    }
    void SetPlayerText()
    {
        playerText.text = targetFloor.ToString();
    }
}
